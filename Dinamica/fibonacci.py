import time


# Variais globais para contagem de chamadas e iterações realizadas por cada método
calls = 0
iterations = 0


# Retorna o n-ésimo (começando em 0) número de Fibonacci usando um método recursivo
def recursiveFibonacci(n):
    global calls
    calls += 1
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return recursiveFibonacci(n - 1) + recursiveFibonacci(n - 2)


# Retorna o n-ésimo (começando em 0) número de Fibonacci usando um método iteratívo / programação dinâmica
def iterativeFibonacci(n):
    global iterations
    if n == 0 or n == 1:
        iterations += 1
        return n
    else:
        vector = [0, 1]
        for i in range(2, n + 1):
            iterations += 1
            vector.append(vector[i - 1] + vector[i - 2])
        return vector[n]


# Executa o algorítmo recursivo de Fibonacci e imprime algumas informações
def computeRecursiveFibonacci(n):
    start = time.time()
    f = recursiveFibonacci(n)
    end = time.time()
    runtime = end - start
    print("n = {}, fibonacci(n) = {}, chamadas à função = {}, tempo de execução = {}".format(n, f, calls, runtime))


# Executa o algorítmo iterativo de Fibonacci e imprime algumas informações
def computeIterativeFibonacci(n):
    start = time.time()
    f = iterativeFibonacci(n)
    end = time.time()
    runtime = end - start
    print("n = {}, fibonacci(n) = {}, iterações = {}, tempo de execução = {}".format(n, f, iterations, runtime))
  

print("\nRecursivo:\n")
computeRecursiveFibonacci(35)
print("\nIterativo:\n")
computeIterativeFibonacci(200)


# Conferir as respostas:
# https://miniwebtool.com/list-of-fibonacci-numbers/?number=201

# OBS: Os tempos de execução não estão sendo calculados de forma rigorosa, este modo fornece apenas uma
# ideia/aproximação dos mesmos.