# Somatório dos valores de determinados índices de um vetor
def sumVectorByIndexes(indexes, vector):    
    total = 0
    for i in indexes:
        total += vector[i]
    return total


# Cria e ordena um vetor em ordem decrescente de valor/peso
def valuePerWeightIndexVector(values, weights):
    vector = []
    for i in range(1, len(values)):
        vector.append([i, values[i], weights[i]])
    vector.sort(key = valuePerWeightOfItem, reverse = True)
    return vector


# Retorna a razão valor-peso de um item.
def valuePerWeightOfItem(item):
    return item[1] / item[2]


# Usa a estratégia gulosa maior valor/peso para resolver o problema da mochila
def greedyKnapsack01(vector, W):
    items = []
    totalWeight = 0
    for item in vector:
        if totalWeight + item[2] <= W:
            items.append(item[0])
            totalWeight += item[2]
        if totalWeight == W:
            break
    return items


# Executa o algorítmo guloso da mochila 0-1 e imprime algumas informações
def computeGreedyKnapsack01(values, weights, W):
    vector = valuePerWeightIndexVector(values, weights)
    items = greedyKnapsack01(vector, W)
    totalValue = sumVectorByIndexes(items, values)
    totalWeight = sumVectorByIndexes(items, weights)
    print("Itens: {}, Valor Total: {}, Peso Total: {}".format(items, totalValue, totalWeight))