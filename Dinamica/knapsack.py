import gknapsack as gks

# Cria uma matriz LINES x COMLUMNS preenchida com CHAR
def createMatrix(lines, columns, char):
    return [[char for column in range(columns)] for line in range(lines)] 


# Imprime a matriz de subproblemas
def printKsMatrix(ksmatrix):
    print("Matriz:\n")
    maxSize = len(str(ksmatrix[-1][-1]))
    for line in ksmatrix:
        for cell in line:
            print("{:^{x}}".format(cell, x = maxSize), end = ' ')
        print("")

# Somatório dos valores de determinados índices de um vetor
def sumVectorByIndexes(indexes, vector):    
    total = 0
    for i in indexes:
        total += vector[i]
    return total


# Procura quais itens compõem as escolha ótima conforme estratégia
def traceback(ksmatrix, weights):
    w = len(ksmatrix[0]) - 1
    j = len(ksmatrix) - 1
    items = []
    while j != 0 and w != 0:
        if ksmatrix[j][w] == ksmatrix[j - 1][w]:
            j -= 1
        else:
            w -= weights[j]
            items.append(j)
            j -= 1
    return items


# Cria uma matriz (Capacidade + 1) x (Itens + 1) e a preenche conforme estratégia
def knapsack01(values, weights, W):
    I = len(values)
    c = createMatrix(I, W + 1, 0)
    for j in range (1, I):
        for w in range (1, W + 1):
            if weights[j] > w:
                c[j][w] = c[j - 1][w]
            else:
               c[j][w] = max(c[j - 1][w], c[j - 1][w - weights[j]] + values[j])
    return c


# Executa o algorítmo da mochila 0-1 e imprime algumas informações
def computeKnapsack01(values, weights, W):
    ksmatrix = knapsack01(values, weights, W)
    printKsmatrix(ksmatrix)
    items = traceback(ksmatrix, weights)
    totalValue = sumVectorByIndexes(items, values)
    totalWeight = sumVectorByIndexes(items, weights)
    print("\nItens: {}, Valor Total: {}, Peso Total: {}".format(items, totalValue, totalWeight))


# Lista de valores, pesos e capacidade da mochila.
# OBS: Um item de valor e peso 0 é adicionado ao início das listas para que os índices das linhas
# na matriz de solução coincidam com a ordem dos itens (1º, 2º, 3º item ...), tornando a leitura
# e a compreensão do algoritmo mais natural e intuitiva.

values = [0, 500, 400, 300, 450]
weights = [0, 4, 2, 1, 3]
W = 5

print("\nDinâmico:\n")
computeKnapsack01(values, weights, W)
print("\nGuloso:\n")
gks.computeGreedyKnapsack01(values, weights, W)

