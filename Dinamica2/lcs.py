maxLCSData = {"length" : 0, "i" : 0, "j" : 0}

# Cria uma matriz LINES x COMLUMNS preenchida com CHAR
def createMatrix(lines, columns, char):
    return [[char for column in range(columns)] for line in range(lines)] 


# Imprime a matriz de subproblemas
def printLcsMatrix(lcsMatrix, X, Y):
    print("Matriz:\n")
    maxSize = len(str(lcsMatrix[-1][-1]))
    print("{:{x}}".format("", x = maxSize + 1), end = '')
    for j in range(len(Y)):
        print("{:^{x}}".format(Y[j], x = maxSize), end = ' ')
    print("")
    i = 0
    for line in lcsMatrix:
        print("{:^{x}}".format(X[i], x = maxSize), end = ' ')
        i += 1
        for cell in line:
            print("{:^{x}}".format(cell, x = maxSize), end = ' ')
        print("")


# Atualiza o tamanho e os indices finais da LCS
def updateMaxLCSData(length, i, j):
    global maxLCSData
    if length >= maxLCSData["length"]:
        maxLCSData["length"] = length
        maxLCSData["i"] = i
        maxLCSData["j"] = j


# Cria uma matriz (|X| + 1) x (|Y| + 1) e a preenche conforme estratégia
def lcsLength(X, Y):
    m = len(X)
    n = len(Y)
    c = createMatrix(m, n, 0)
    for i in range(1, m):
        for j in range(1, n):
            # COMPLETE AQUI
                
            

            updateMaxLCSData(c[i][j], i, j)
    return c


# Procura quais caracteres compõem a subsequência máxima conforme estratégia
def traceback(lcsMatrix, X):
    global maxLCSData
    i = maxLCSData["i"]
    j = maxLCSData["j"]
    lcs = ""
    while lcsMatrix[i][j] != 0:
        # COMPLETE AQUI





        
    return lcs


# Executa o algorítmo LCS e imprime algumas informações
def computeLCS(X, Y):
    lcsMatrix = lcsLength(X, Y)
    printLcsMatrix(lcsMatrix, X, Y)
    lcs = traceback(lcsMatrix, X)
    totalLenght = len(lcs)
    print("\nTamanho da subsequência máxima: {}, Subsequência máxima: {}".format(totalLenght, lcs))


# Cadeia de caracteres X e Y
# OBS: Um caracter X e Y são adicionados ao início das respectivas cadeias para que os índices das linhas
# na matriz de solução coincidam com a ordem dos caracteres (1º, 2º, 3º item ...), tornando a leitura
# e a compreensão do algoritmo mais natural e intuitiva.
X = "XESTUDAR"
Y = "YPROCRASTINAR"
computeLCS(X, Y)

"""
# Teste mais legal =D:
X = "XACCGGTCGAGTGCGCGGAAGCCGGCCGAA"
Y = "YGTCGTTCGGAATGCCGTTGCTCTGTAAA"
computeLCS(X, Y)
"""
